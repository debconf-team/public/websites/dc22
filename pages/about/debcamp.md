---
name: DebCamp
---

# What is DebCamp

We received some questions about "what is DebCamp" and "how is DebCamp different
from [DebConf](https://www.debconf.org)". We wrote this text to help you decide
whether to come early to Pritzen to take part in DebCamp from July 10th to 16th
(the week before DebConf22).

DebCamp is the hacking session right before DebConf. DebCamp usually occurs in
the week before DebConf itself. It's a week for Debian contributors to focus on
their Debian-related projects, tasks, or problems uninterrupted.

DebCamps are largely self-organized since it is for people who would like to do
some work. Some prefer to work alone, while some prefer to participate in or
organize [sprints](https://wiki.debian.org/Sprints). Both are great, although we
strongly encourage you to plan your DebCamp week in advance.

If you are around and looking for something to do, please get in touch with the
DebConf organization team on the #debconf-team IRC channel. You can also find
more information under the
[volunteers' section](https://debconf22.debconf.org/volunteers) on the main
DebConf22 page.

We will not have a regular schedule with talks, workshops, and BoFs during
DebCamp (from 10th to 16th this year). The regular schedule will happen during
DebConf22 (from 17th to 24th). You can take a look at
[this page](https://wiki.debian.org/DebConf/19/DebCamp) to know what happened
during DebCamp in 2019.

