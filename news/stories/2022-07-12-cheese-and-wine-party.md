---
title: DebConf22 Cheese and Wine Party
---

In less than five days we will be in Prizren to start
[DebConf22][debconf22] \o/

This C&W is the 18th official DebConf Cheese and Wine party. The first C&W was
improvised in Helsinki during DebConf 5, in the so-called "French" room. Cheese
and Wine parties are now a tradition for DebConf.

The event is very simple: bring good edible stuff from your country. We like
cheese and wine, but we love the surprising stuff that people bring from all
around the world or regions of Kosovo. So, you can bring non-alcoholic drinks
or a typical food that you would like to share as well. Even if you don't
bring anything, feel free to participate: our priorities are our attendants
and free cheese.

We have to organize for a great party. An important part is planning - We want
to know what you are bringing, in order to prepare the labels and organizing
other things.

So, please go to our
[wiki page][wikipage]
and add what you will bring!

If you don't have time to buy before travel, we list some places where you can
buy cheese and wine in. There are more information about C&W, what
you can bring, vegan cheese, Kosovo customs regulations and non-alcoholic
drinks at [our site][cheese].

C&W will happen on July 19th, 2022 (Tuesday) after 19h30min.

We are looking forward to seeing you all here!

[debcamp]: https://wiki.debian.org/DebConf/22/DebCamp
[debconf22]: https://debconf22.debconf.org
[wikipage]: https://wiki.debian.org/DebConf/22/CheeseWine#What_will_be_available_during_the_party.3F
[cheese]: https://debconf22.debconf.org/about/cheese-and-wine-party
