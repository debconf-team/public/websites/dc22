from django.conf.urls import include, url
from dc22.views import now_or_next


urlpatterns = [
    url(r'^now_or_next/(?P<venue_id>\d+)/$', now_or_next, name="now_or_next")
]
