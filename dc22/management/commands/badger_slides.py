from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from wafer.talks.models import Talk, ACCEPTED


FROM = 'content@debconf.org'

SUBJECT = 'Please upload your DebConf22 slides to Salsa'
BODY = '''\
Dear conference speaker,

Did you use slides or some kind of visual support for the talk you gave at
DebConf22?

If possible, we would like you to upload them to this git LFS repository:

git@salsa.debian.org:debconf-team/public/share/debconf22.git

You can find documentation on how to do so and where to put them at the
following link:

https://salsa.debian.org/debconf-team/public/share/debconf22/-/tree/main#uploading-slides

If you find this process too complex, you can always send us your slides at
<debconf-video@lists.debian.org> and we'll be more than happy to upload them
for you.

Cheers and thanks for taking the time to speak at DebConf22!

The DebConf Video Team
'''


class Command(BaseCommand):
    help = "Ask speakers to upload their slides"

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),

    def badger(self, talk, emails, dry_run):
        try:
            scheduleitem = talk.scheduleitem_set.get()
        except ObjectDoesNotExist:
            return

        if dry_run:
            print('I would badger speakers of: %s'
                  % talk.title.encode('utf-8'))
            return
        email_message = EmailMultiAlternatives(
            SUBJECT, BODY, from_email=FROM, to=emails)
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        already_badgered = set()
        for talk in Talk.objects.all():
            if talk.status == ACCEPTED:
                emails = [user.email for user in talk.authors.all()]
                emails = list(set(emails) - already_badgered)  # remove emails already badgered
                self.badger(talk, emails, dry_run)
                for user in talk.authors.all():
                    already_badgered.add(user.email)
