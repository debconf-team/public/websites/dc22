from django import template
from django.utils.timezone import now

from wafer.schedule.models import ScheduleBlock

register = template.Library()


@register.filter
def stream(venue):
    raise NotImplementedError(f"No video stream for {venue.name}")


@register.filter
def channel(venue):
    raise NotImplementedError(f"No IRC channel for {venue.name}")


@register.simple_tag
def current_block_id():
    """returns the ID of the current schedule block"""
    try:
        current_block = ScheduleBlock.objects.get(
            start_time__lte=now(), end_time__gt=now())
    except ScheduleBlock.DoesNotExist:
        return None
    else:
        return current_block.id
